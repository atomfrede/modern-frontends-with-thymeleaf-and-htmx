package com.gitlab.atomfrede.todomvc.htmxoob.todo_item.web;

import com.gitlab.atomfrede.todomvc.htmxoob.todo_item.TodoItem;
import com.gitlab.atomfrede.todomvc.htmxoob.todo_item.TodoItemNotFoundException;
import com.gitlab.atomfrede.todomvc.htmxoob.todo_item.TodoItemRepository;
import io.github.wimdeblauwe.htmx.spring.boot.mvc.HtmxResponse;
import io.github.wimdeblauwe.htmx.spring.boot.mvc.HxRequest;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class TodoItemController {

    private TodoItemRepository repository;

    public TodoItemController(TodoItemRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public String index(Model model) {
        addAttributesForIndex(model, ListFilter.ALL);
        return "index";
    }

    @GetMapping("/active")
    public String indexActive(Model model) {
        addAttributesForIndex(model, ListFilter.ACTIVE);
        return "index";
    }

    @GetMapping("/completed")
    public String indexCompleted(Model model) {
        addAttributesForIndex(model, ListFilter.COMPLETED);
        return "index";
    }

    @PostMapping
    @HxRequest
    public HtmxResponse htmxAddTodoItem(TodoItemFormData formData,
                                        Model model) {
        TodoItem item = repository.save(new TodoItem(repository.nextId(), formData.getTitle(), false));
        return getHtmxResponseForTodoItem(model, item);
    }

    @PutMapping("/{id}/toggle")
    @HxRequest
    public HtmxResponse htmxToggleTodoItem(@PathVariable("id") Long id,
                                           Model model) {
        TodoItem todoItem = repository.findById(id)
                .orElseThrow(() -> new TodoItemNotFoundException(id));

        todoItem.setCompleted(!todoItem.isCompleted());
        repository.save(todoItem);

        return getHtmxResponseForTodoItem(model, todoItem);
    }

    @DeleteMapping("/{id}")
    @HxRequest
    public HtmxResponse htmxDeleteTodoItem(@PathVariable("id") Long id,
                                           Model model) {
        repository.deleteById(id);

        return activeItemsCountFragment(model);
    }

    @GetMapping("/active-items-count")
    @HxRequest
    public String htmxActiveItemsCount(Model model) {
        model.addAttribute("numberOfActiveItems", getNumberOfActiveItems());

        return "fragments :: active-items-count";
    }

    private HtmxResponse getHtmxResponseForTodoItem(Model model,
                                                    TodoItem item) {
        model.addAttribute("item", toDto(item));

        return new HtmxResponse()
                .addTemplate("fragments :: todoItem")
                .and(activeItemsCountFragment(model));
    }

    private HtmxResponse activeItemsCountFragment(Model model) {
        model.addAttribute("numberOfActiveItems", getNumberOfActiveItems());
        return new HtmxResponse()
                .addTemplate("fragments :: active-items-count");
    }

    private void addAttributesForIndex(Model model,
                                       ListFilter listFilter) {
        model.addAttribute("item", new TodoItemFormData());
        model.addAttribute("filter", listFilter);
        model.addAttribute("todos", getTodoItems(listFilter));
        model.addAttribute("totalNumberOfItems", repository.count());
        model.addAttribute("numberOfActiveItems", getNumberOfActiveItems());
        model.addAttribute("numberOfCompletedItems", getNumberOfCompletedItems());
    }

    @PostMapping
    public String addNewTodoItem(@Valid @ModelAttribute("item") TodoItemFormData formData) {
        repository.save(new TodoItem(repository.nextId(), formData.getTitle(), false));

        return "redirect:/";
    }

    @PutMapping("/{id}/toggle")
    public String toggleSelection(@PathVariable("id") Long id) {
        TodoItem todoItem = repository.findById(id)
                .orElseThrow(() -> new TodoItemNotFoundException(id));

        todoItem.setCompleted(!todoItem.isCompleted());
        repository.save(todoItem);
        return "redirect:/";
    }

    @PutMapping("/toggle-all")
    public String toggleAll() {
        Iterable<TodoItem> todoItems = repository.findAll();
        for (TodoItem todoItem : todoItems) {
            todoItem.setCompleted(!todoItem.isCompleted());
            repository.save(todoItem);
        }
        return "redirect:/";
    }

    @DeleteMapping("/{id}")
    public String deleteTodoItem(@PathVariable("id") Long id) {
        repository.deleteById(id);

        return "redirect:/";
    }

    @DeleteMapping("/completed")
    public String deleteCompletedItems() {
        List<TodoItem> items = repository.findAllByCompleted(true);
        for (TodoItem item : items) {
            repository.deleteById(item.getId());
        }
        return "redirect:/";
    }

    private List<TodoItemDto> getTodoItems(ListFilter filter) {

        return switch (filter) {
            case ALL -> convertToDto(repository.findAll());
            case ACTIVE -> convertToDto(repository.findAllByCompleted(false));
            case COMPLETED -> convertToDto(repository.findAllByCompleted(true));
        };
    }

    private List<TodoItemDto> convertToDto(Iterable<TodoItem> todoItems) {
        List<TodoItemDto> results = new ArrayList<>();
        todoItems.forEach(it -> results.add(toDto(it)));
        return results;
    }

    private TodoItemDto toDto(TodoItem todoItem) {
        return new TodoItemDto(todoItem.getId(),
                todoItem.getTitle(),
                todoItem.isCompleted());
    }

    private int getNumberOfActiveItems() {
        return repository.countAllByCompleted(false);
    }

    private int getNumberOfCompletedItems() {
        return repository.countAllByCompleted(true);
    }

    public static record TodoItemDto(long id, String title, boolean completed) {
    }

    public enum ListFilter {
        ALL,
        ACTIVE,
        COMPLETED
    }
}
